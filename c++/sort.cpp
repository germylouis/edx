/*
Germy Louis
February 27,2016
Assignment #5:Hammering Sort
COP 3014
*/
#include <iostream>
#include <iomanip>

using namespace std;
//declaring funtions that order the numbers in array using the hammering sort and a function that takes the averages on the input
void order(int[], int);
void average(int[], int);



int main()//main function
{
    
    const int SIZE = 100;		//setting size of array
    int darray[SIZE];		//creating array
    cout<<"Enter a series of positive numbers"<<endl;
    cout<<"Enter a negative number to quit"<<endl;
    int input;
    int count = -1;
    do{//do loop so that i can tell if the user inputs do not meet the standards
        
        cin >> input;
        if(input >= 0){
            count++;
            darray[count] = input;//user input into array
        }
        
        else{
            cout << "You are required to enter a minimum of two numbers."
                 <<"Continue entering input.\n";
        }//in case user does not put enough integers
        
    }while( (input >= 0 && count < SIZE)||(count < 1) );
    //checking for standards to be met
    order(darray, count);//calling hammering sort function
    cout << endl;
    average(darray,count);//calling for the average
    
    return 0;//end of main
}



void average(int arr[], int count)
{//definition of the average function
    int sum = 0,
        ave;
    
    for (int i=0;i<=count;i++)
        sum += arr[i];
    //adding all the numbers in array
    ave = sum/(count+1);//finding the average
    cout << "AVERAGE: "<< ave << endl;
}



void order(int darray[], int count)//deifining reverse funtion
{//function definition for hammering sort code
    int temp;
    for(int i = 0; i < count; i++){//complex code using the gudlines on blackboard
        if(darray[i] > darray[i+1]){
            temp = darray[i];
            darray[i] = darray[i+1];
            darray[i+1] = temp;
            for(int k = i; k >= 1; k--){
                if(darray[k-1] > darray[k]){
                    temp = darray[k];
                    darray[k] = darray[k-1];
                    darray[k-1] = temp;
                }
            }
        }
    }

    cout <<"\nSorted List\n";//printing the array
    for (int i=0; i <= count;i++)
        cout<<darray[i]<<endl;
}//the end
// ***************************************************************************************
// * Function Name: blob																 *
// * Description: Reads in file and prints out how many blobs or "pictures" there are.   *
// * Parameter Description: List each parameter, type, and								 *
// * Date: 04/22/2016																	 *
// * Author: Germy Felder Louis															 *
// * Referenes: COP 3014 Textbook													     *
// ***************************************************************************************
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

const int row = 22;		//row for array
const int column=72;	//column for array
void Recursive( char [][column], int, int);

int main()
{
//blob counter, string to hold line of array and initial filling of array with spaces
int bcount=0;
string l;
char blob[row][column];
for(int i=0;i<row;i++)
		{
		for(int j=0;j<column;j++)
			{
			blob[i][j]=' ';
			}
}
ifstream myfile;
myfile.open("blob.txt");
//filling 2d array along with border
for (int rows = 1; rows <= 20; rows++){
	getline(myfile, l);
	for (int columns = 1; columns <= 70; columns++){
		blob[rows][columns] = l[columns-1];
	}
}
//counting blobs
for(int r = 0 ; r < row; r++){
	for(int c = 0;c < column; c++){
		if( blob[r][c]=='X' )
			bcount++;
			Recursive(blob,r,c);
	}	
}
//printing amount of blobs
cout<<"Number of blobs: "<<bcount<<endl;
return 0;
}
// ******************************************************************************************************
// * Function Name: recursive function																	*
// * Description: deletes x's as it searches for it recursively											*
// * Parameter Description: Passing blob array, along with rows and column from the itteration in Main	*
// * Referenes:Recitation																				*
// ******************************************************************************************************
void Recursive (char blob[row][column], int r, int c)
{
	if( blob[r][c]=='X' ){
	blob[r][c]=' ';
	if(blob[r][c+1]=='X')
	Recursive (blob,r,c+1);
	if(blob[r][c-1]=='X')
	Recursive(blob,r,c-1);
	if(blob[r+1][c]=='X' )
	Recursive(blob,r+1,c);
	if(blob[r-1][c]=='X' )
	Recursive(blob,r-1,c);
	if(blob[r-1][c-1]=='X')
	Recursive(blob,r-1,c-1);
	if(blob[r+1][c+1]=='X')
	Recursive(blob,r+1,c+1);
	if(blob[r-1][c+1]=='X')
	Recursive(blob,r-1,c+1);
	if(blob[r+1][c-1]=='X')
	Recursive(blob,r+1,c-1);
	}
}
/*GermyLouis
April 8,2016
COP3014
Assignment#8: CakeorPie
*/
#include <iostream>
#include<iomanip>
#include <string>
using namespace std;
#include <fstream>
//struct given
struct Roster {
string LName;  //last name of list
string FName;  //first name of list
char Cheesecake;  //cheesecake or  nah
};
struct Roster*MyRoster;
int getsize(string);  //gets size of the file
void sort(Roster [],int,string);  //bubble sorts the array
int cakeorpie(Roster [],int,int&,int&);  //determines how many cake or pies
int main() //main function
{
int count=0,count2=0,pcount=0, ccount=0;  //counters
string filename;

cout<<"Please enter a valid file name: "<<endl;
cin>>filename;  //asking for file

    ifstream myfile(filename);
    if (myfile.is_open())
    {
        cout<<"cheesecake found"<<endl;
    }
   
		while(!myfile.is_open()){
        cout<<"cheesecake not found"<<endl;
		cout<<"Please enter a valid file name: "<<endl;
		cin>>filename;
		myfile.open(filename);
		}
	
	count=getsize(filename);  //calling for size of file
	MyRoster = new Roster[count];
	cout<<"\t"<<"The Cheesecake Report"<<endl; //the report
	cout<<"Last Name"<<"\t"<<"First Name"<<"\t"<<right<<"\tCake or Pie"<<endl;
	cout<<"---------"<<"\t"<<"---------"<<"\t"<<"\t---------"<<endl;
	while(!myfile.eof()&&count2<count) //filling array
	{
		myfile>>MyRoster[count2].LName;
		myfile>>MyRoster[count2].FName;
		myfile>>MyRoster[count2].Cheesecake;
		count2++;
		
	}
	sort(MyRoster,count,filename);  //sorts array in alphabetical order
	for (int j=0;j<count;j++) //printing and formatting array
	{
		cout<<right<<MyRoster[j].LName<<"\t \t"<<internal<<MyRoster[j].FName<<setw(15);
		if(MyRoster[j].Cheesecake=='p'||MyRoster[j].Cheesecake=='P')
			cout<<"\t"<<"\tPie"<<left<<endl;
		else if (MyRoster[j].Cheesecake=='c' ||MyRoster[j].Cheesecake== 'C')
			cout<<"\t"<<"\tCake"<<left<<endl;
	}
	cout<<endl;
	cakeorpie(MyRoster,count,pcount,ccount); //counts how many cakes or pies
	cout<<"Number of Records:  "<<count<<endl;	
	cout<<"Number of people who believe cheesecake is pie: "<<pcount<<endl;
	cout<<"Number of people who believe cheesecake is cake:"<<ccount<<endl;
	return 0;

}
int getsize(string name) //gets size for dynamic allocation
{
int count = 0;   //code written with the help of my T.A.
fstream myfile;
myfile.open(name);
    char line[1024],line2[1024];
    while(!myfile.eof())
    {
        myfile.getline(line,1024);
        count++;
    }
	return count;
}
void sort(Roster MyRoster[],int count,string name)  //bubble sort definition found in COP3014 textbook
{
fstream myfile;
int cont=0;
string s,s1;
char s2;
bool swap;
do
{
	swap=false;
	for(int i=0;i<(count-1);i++)  //loops until value stays false
	{
		if(MyRoster[i].LName.compare(MyRoster[i+1].LName)>0)
		{
			s = MyRoster[i].LName;
			s1 = MyRoster[i].FName;
			s2 = MyRoster[i].Cheesecake;

			MyRoster[i].LName=MyRoster[i+1].LName;
			MyRoster[i].FName=MyRoster[i+1].FName;
			MyRoster[i].Cheesecake=MyRoster[i+1].Cheesecake;

			MyRoster[i+1].LName = s;
			MyRoster[i+1].FName	= s1;
			MyRoster[i].Cheesecake = s2;
			swap =true;
		}
	}
  }while(swap);
//cout<<s<<" "<<s1;
}
int cakeorpie(Roster MyRoster[],int count,int& p,int& c)  //number of cake and pies function definition
{
	for(int i=0;i<count;i++)
	{
		if(MyRoster[i].Cheesecake=='p'||MyRoster[i].Cheesecake=='P')
		{
			p++;
		}
		if(MyRoster[i].Cheesecake=='c'||MyRoster[i].Cheesecake=='C')
		{
			c++;
		}
	}
	return p,c;
} //returns two ints

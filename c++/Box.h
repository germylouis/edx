class Box {
public:
	Box (  int=0 , char='#' , char='*' );	//constrctor

	void setBox(int, char,char);	//setting box
	void setlength (int);		//setting the get lengths
	void SetBorder(char);	//setting the border
	void SetFill(char);	//setting the inside filling

	int GetSize();
	int Perimeter();
	int Area();

	void Summary();
	void Draw();
	void Shrink();
	void Grow();

private://private variables to be accessed
	int size;
	char border;
	char filling;

};
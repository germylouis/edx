//
//  main.cpp
//  Germy#2
//
//  Created by Germy Louis on 1/29/16.
//  Copyright © 2016 Connor Davis. All rights reserved.
//

#include <iostream>
#include <cmath>
using namespace std;
void menloop();
void cube();
void natural();
void abval();
void inverse();
void square();





int main()
{
    int input;
    do
    {
        cout<<""<<endl;
        menloop();
        cin >> input;
        
        
        switch (input)
        {
            case 1:
                square();
                break;
            case 2:
                cube();
                break;
            case 3:
                natural();
                break;
            case 4:
                inverse();
                break;
            case 5:
                abval();
                break;
                
            default:
                cout << "Goodbye!";
                cout<<endl;
                
        }
        
        
    }while(input >=1 && input<=5);
    return 0;
}
void menloop()
{
    
    cout << "			Menu" << endl;
    cout << "	1 - Calculate Square Root" << endl;
    cout << "	2 - Calculate Cube"<<endl;
    cout << "	3 - Calculate Natural Logarithm" << endl;
    cout << "	4 - Calculate Inverse" << endl;
    cout << "	5 - Calculate Absolute Value" << endl;
    cout << "	0 - Exit Program" << endl;
    cout << "----------------------------------------------------" << endl;
    cout << "Enter Menu Option:";
    
}
void square()
{
    int number;
    
    cout << "Enter in a non - negative decimal number : ";
    cin >> number;
    cout << "The square root of " << number << " is " << sqrt(number) << endl;
}
void cube()
{
    int number;
    cout << "	Enter in a decimal number :";
    cin >> number;
    cout << endl;
    cout << "The Cube of " << number << " is " << pow(number,3) << endl;
}
void natural()
{
    double number;
    cout << "Enter in a decimal number :";
    cin >> number;
    cout << endl;
    cout << "The Natural Log of " << number << " is " << log(number) << endl;
}
void inverse()
{
    double number;
    cout << "Enter in a decimal number :";
    cin >> number;
    cout << endl;
    cout << "The Inverse of " << number << " is " <<1/number <<endl ;
    
}
void abval()
{
    double number;
    cout << "Enter in a decimal number : " << endl;
    cin >> number;
    cout << "The Absolute Value of " << number << " is" << fabs(number) << endl;
}

/* Name: (Germy	Louis) 
Date: (10/19/2015) 
Section: (12) 
Assignment: (Assignment#4) 
Due Date: (10/19/2015) 
About this project: The goal of this assignment is to have a program that runs a dicegame that rolls two dies and if the sum is 7 money is won. If not, the opposite occurs until total money equates to zero. 
Assumptions: Givern correct user input the output should be the amount of money the user has gained and loss and the type of rolls the user is getting. 

All work below was performed by (Germy Louis) */
#include <iostream>
#include <cstdlib>		
#include <ctime>	//this and the previous one is used for the random generator
using namespace std;

void welcome();
void roll(int &, int &);
bool winOrLose(int &, int &);		//declaring functions

int main ()			//declaring main
{
    const int initial = 500;	//constant for money
    int da, db, sum,
        bet = 0,
        remain = initial;
    srand( time(0) );
    
	welcome();		//calling the welcome and displaying all the other initial text
    do{
        cout<<"You have $" << remain << " remaining\n";
        cout<<"Place your bet: ";
        cin >> bet;
        
        if(bet == 0)		//for invalid entry
            cout << "You can't bet 0!\n\n";
        
        else if(bet > remain)		//also for invalid entry
            cout << "You can't bet more $ than you have!\n\n";
        
        else if(bet > 0){
            
            roll(da, db);
            sum = da + db;
            cout << "You rolled " << da << " and " << db << " = " << sum << endl;
            
            if(sum == 7){
                cout << "You Win!!\n\n";
                remain+= bet;
            }
            
            else{
                cout << "You Lose!!\n\n";
                remain-= bet;
            }
        }
        
    }while(bet >= 0 && remain > 0);
    
    if(remain == 0)
        cout << "You're broke!";//prints out when user runs out of money
    
    else{
        cout << "\nYou're leaving with $" << remain << endl;//when user quits shows what they are leaving with
        
        if(remain < initial)
            cout << "You LOST $" << initial - remain;//showing user what they lost at the end

        else if(remain > initial)//showing what user made at the end
            cout << "You MADE $" << remain - initial;
        
        else
            cout << "You BROKE EVEN so you still have your initial $" << initial;
    }
    
    cout << "\nThanks for playing!\n";
    
    
		 
	return 0;
}

void welcome (){//deifining welcome
    
    cout << "***** Welcome to SEVENS! *****\n"
         << "Wager whatever you'd like. If you roll a 7, you win whatever you bet! \n"
         << "But beware, if you don't roll a 7, you lose your wager\n"
         << "Enter a negative digit to quit \n"
         << "LETS BEGIN!!!\n\n";
}

void roll (int &r, int &r2){//defining roll
    
    r = rand() % 6 + 1;
    r2 = rand() % 6 + 1;
								//limitation on die numbers
}

bool winOrLose (int& roll, int& roll2){          //defining rol
    
	if(roll + roll2 == 7)
        return true;				//checks if die equates to seven returns true else false

	return false;

}

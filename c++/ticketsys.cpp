/*Germy Louis
COP 3014 Section 12
Project Number 1
This program calculates the ticket pricing of adults, children and seniors, uses the information 
inputed by user, and calculates a price with a seven percent tax included.*/	
#include <iostream>
using namespace std;

int main()
{
	int atick , ctick , stick ;						//number of tickets
	const double adult = 28.5 , child = 12.5, senior = 16.5;		//ticket prices assigned
	double sub , tax , total;								//subtotal, tax and total initialized as doubles to give exact prices.

	cout << "Welcome to the FSU Football ticket system!" << endl;
	cout <<"Please enter number of: " << endl;
	cout << "	Adults" << endl;
	cout << "	Children (Up to age 12)" << endl;
	cout << "	Seniors(60+)" << endl;
	
	cout << "Adults --> "; //prompting user for number of adult tickets wanted
	cin >> atick;
	cout <<endl;

	cout << "Children -->";   //prompting users for amount fo chikdren tickets wanted
	cin >> ctick;		//storing user input in variable
	cout <<endl;

	cout << "Seniors -->" ;		//promting user for amount of senior tickets wanted
	cin >> stick;
	cout <<endl;

	sub = (atick*adult) + (ctick*child) + (stick*senior);		//trying to calculate tax in complicated way
	tax = sub * 0.07;		//storing subtotal of ticket costs into tax to use in formula below
	total = sub + tax;		//tax calculated earlier to reach total price here
	cout <<"-------------------------------" <<endl;
	cout <<"TOTAL (before tax) = $" << sub << endl;
	cout <<"Tax = $" << tax << endl;
	cout <<"TOTAL COST (after tax) =  $" << total << "" << endl;


	cout << "Thanks for using the ticket system!" <<endl;
		
	
	return 0;
}
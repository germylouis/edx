/*
Germy Felder Louis
COP 3014
Assignment #3: Calendar
February 12, 2016
*/
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
void PrintMonth(const int month, const int startday, const bool leap);//declaring function
int main(void)//delcaring main
{
 // ******************************************************************
 // * Print just a bunch of Random months - Assignment #3 *
 // ******************************************************************
 PrintMonth(1,0,false); // Print January 1st, on a Sunday
 cout<<endl;//spacing
 cout<<endl;
 PrintMonth(2,1,true); // Print February 1st leap year, on Monday
 cout<<endl;
 cout<<endl;
 PrintMonth(1,2,false); // Print January 1st, on a Tuesday
  cout<<endl;
  cout<<endl;
 PrintMonth(2,3,false); // Print February 1st not leap, on Wednesday
  cout<<endl;
  cout<<endl;
 PrintMonth(1,4,false); // Print January 1st, on a Thursday
  cout<<endl;
  cout<<endl;
 PrintMonth(2,5,false); // Print February 1st, on a Friday
  cout<<endl;
  cout<<endl;
 PrintMonth(1,6,false); // Print January 1st, on a Saturday
  cout<<endl;
  cout<<endl;
 PrintMonth(6,1,false); // Print June 1st, on Monday
  cout<<endl;
  cout<<endl;
 PrintMonth(12,4,false); // Print December 12th, on a Thursday

return 0;
}
void PrintMonth(const int month, const int startday, const bool leap)//defining function
{
	string days[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};//putting my days and months into string arrays to help with spacing
	const string montharray[13]={"hi","January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "December"};
	
	//my way of indenting the month
	cout<<setw(25)<<montharray[month]<<endl;
	for(int i=0;i<7;i++){	//printing out array of days
		cout<<days[i]<<setw(6);			//spacing
			}
	cout<<setw(startday*6)<<endl;
	if(month==2){		//if month is february
		for(int i=1;i<=28; i++){
		cout<<i<<setw(6);
		if(i==7-startday || i==14-startday || i==21-startday|| i==28-startday)
			cout<<setw(0)<<endl;
		}
	}
	else if(month==2 && leap == true)	//if month is february and it is a leap year
	{
			for(int i=1;i<=29; i++){		//prints 29 days
			cout<<i<<setw(6);
			if(i==7-startday || i==14-startday || i==21-startday|| i==28-startday)
			cout<<setw(0)<<endl;
			}
	}
	else{
	 for(int i=1;i<=31; i++){   //prints 31 days for everything else
		cout<<i<<setw(6);
	if(i==7-startday || i==14-startday || i==21-startday|| i==28-startday) //making sure i am taking into account when the month begins and how it affects the rest of the days
			cout<<setw(0)<<endl;
				if(startday==5|| startday == 6 && i==29)
				cout<<setw(0)<<endl;
	 }



}
}
/* Name: (Germy Louis)
Date: (10/27/2015)
Section: (12)
Assignment: (Assignment#6)
Due Date: (10/30/2015 11:59 PM)
About this project: (Goal of this assignment is to practice with arrays and functions. It will take 15 integer values and store it into an array. You can 
either reveres the array, multiply all the values in the array by a specific number, add odd numbers, delete or insert numbers.)
Assumptions: (I assume that this program will take any integer value inputed by user and do all the functions that it saysit will do.)
All work below was performed by (Germy Louis) */

#include <iostream>
using namespace std;
/*Given Function prototypes*/
void PrintArray (const int arr[], const int size);
void FillArray(int arr[], const int size);
void insert(int arr[], const int size, int , int );
void Delete(int[], int, int);
void reverse(int[], int);
void timeswhat(int [], int, int);
int sumodds(const int[], int, int);
void showmenu();
int main()									//all functions declared
{
   const int SIZEE = 15;		//setting size of array
   int number[SIZEE];		//creating array
   char select;		//for character inputs
	
   /* Declare your array and write the menu loop */

   showmenu();	//display menu before loop
		   do{		//opted for do while plus cases and swithces
		cout<<endl;
	   cout << "Enter your menu selection: ";
		cin>>select;

		switch(select)
			{
		  case 'M':
		  case 'm':
			  {
				  showmenu();
			  }break;
	   
		  case 'F':
		  case 'f':
			  {
			 FillArray(number, SIZEE);	//where user fills array
			  } break;

		  case 'P':
		  case 'p':
			  {
			 PrintArray(number, SIZEE);	//will print array for user
			  } break;

		  case 'I':
		  case 'i':
			  {
			 int insertdex, insertval;		// to variables that hold index value and input  value
					cout<<"Enter the value to insert: ";
					cin>>insertval;
					cout<<endl;
					cout<<"Enter index at which to insert: ";
					cin>>insertdex;cout<<endl;

					insert(number, SIZEE, insertval,insertdex);	//calling on insert function
					cout << "The array: \n"<<"  { ";
					for (int i=0; i<SIZEE;i++)		//for loop to display array
						cout<<number[i]<<" ";
					cout << "}"<<endl;
			  }break;

		  case 'D':
		  case 'd':
			  {
			 cout<<"Enter index of item to delete: ";
						int index;
						cin>>index;
						cout<<endl;
						Delete(number,SIZEE,index);	//Calling on delete function
						cout << "The array: \n"<<"  { ";
						for (int i=0; i<SIZEE;i++)
						cout<<number[i]<<" ";
						cout<<"}"<<endl;
			  } break;

		  case 'R':
		  case 'r':
			  {
			 reverse(number, SIZEE);	//calling on reverse function
							cout << "The array: \n"<<"  { ";
							for (int i=0; i<SIZEE;i++)
							cout<<number[i]<<" ";
							cout<<"}"<<endl;
							
		}	break;


		  case 'T':
		  case 't':
			  {
			 int multby;
							cout<<"Enter value for multiplication of array contents:";
							cin>>multby;		//value to mulitply array by
							cout<<endl;
							timeswhat(number, SIZEE, multby);	//calling in timeswhat function
							cout << "The array: \n"<<"  { ";
							for (int i=0; i<SIZEE;i++)
							cout<<number[i]<<" ";
							cout<<"}"<<endl;
		}break;

		  case 'S':
		  case 's':
			  {
			 cout<<"How many odd numbers to add:";
							int odd;
							cin>>odd;
							cout<<endl;
							cout<<"Sum of the first "<<odd<<" odd numbers is: " << sumodds(number, SIZEE, odd);//calling sumodds finction
							cout << endl;
			  }	break;

		  default:  
			 cout << "Invalid selection. Please try again.\n"; //for invali entry
			 cout<<"Enter menu selection: ";
			cin>>select;
			cout<<endl;
		}

   }while(select != 'q' && select != 'Q');	//for some reason you have to enter q or Q twice to quit
		   PrintArray(number, SIZEE);//prints final array when user wants to exit, return 0
   return 0;
   
}					// end main()


void insert(int arr[],  const int size, int newval, int index)//defining insert function
{
	const int tempSize=15;//declaring new constant to set temp array
	
	int temparr[tempSize];//creating temp array
	for (int i =0; i<size; i++)
	{
		temparr[i] = arr[i];//copying original array into temp array
	}

	arr[index]=newval;// replacing with user value thats inputed
	for(int i = index+1; i<size; i++)//setting counter at the index user wants plus one
	{
		arr[i]=temparr[i-1];//taking calued from temp array to essentially shift all the numbers in the original to the right
	}
}
void Delete(int arr[], int size, int indel)//defining delete function
{

	for(int i = indel; i < size - 1; i++)//i equals the index the user iputed
	{
		arr[i] = arr[i+1];//moving all values to the left and deleting the value at index
	}

	arr[size-1] = 0;//setting last "box" in array as 0

}
void reverse(int arr[], int size)//deifining reverse funtion
{
int temp,j=0;//temporary values
	for (int i=0; i<=13; i++)//setting for loop to be one behind the next for loop
	{
		for (j=i+1;j<=14; j++)
		{
		
		if (arr[i] !=0)//this switches all the value so that it is reversed
		{
			temp = arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
		}
		}
	}
}
void timeswhat(int arr[], int size, int multby)			//defining timeswhat function
{
for (int index = 0; index <size; index++)
	arr[index] *= multby;			//simply multiplying the array by whatever the user inputs
}
int sumodds(const int arr[], int size, int odd)//defining sum odds
{
	int sum = 0;
	int count = 0;		//setting sum and count to 0

	for(int i = 0; i < size; i++)
	{
		if ( arr[i] % 2 == 1)		//if the number at that point is odd it is added to sum
		{
			sum += arr[i];
			count++;
		}

		if(count == odd)		//once count equals the number of odds the user wanted to add up, return sum
			return sum;
	}

	return sum;	//return sum if user inout exceeds what is there
}
/* Definitions of PrintArray and FillArray below
DO NOT CHANGE THESE!*/
void PrintArray(const int arr[], const int size)
// this function prints the contents of the array
{
   cout << "\nThe array:\n { ";
   for (int i = 0; i < size-1; i++)	// print all but last item
      cout << arr[i] << ", ";

   cout << arr[size-1] << " }\n";	// print last item
}

void FillArray(int arr[], const int size)
// this function loads the contents of the array with user-entered values
{
   cout << "Please enter " << size << " integers to load into the array\n> ";

   for (int i = 0; i < size; i++)
	cin >> arr[i];			// enter data into array slot
}
void showmenu()		//show menu function for simplcity
{
	   cout<<"\t** Given features **"<<endl;
   cout<<"P"<<"\tPrint the array contents"<<endl;
   cout<<"F"<<"\tFill the array (user entry)"<<endl;
   cout<<""<<endl;
   cout<<"\t** Function Tests **"<<endl;
   cout<<"I"<<"\tInsert"<<endl;
   cout<<"D"<<"\tDelete"<<endl;
   cout<<"R"<<"\tReverse"<<endl;
   cout<<"T"<<"\tTimesWhat"<<endl;
   cout<<"S"<<"\tSumOdds"<<endl;
   cout<<""<<endl;
   cout<<"M"<<"\tPrint this menu"<<endl;
   cout<<"Q"<<"\tQuit this program"<<endl;
   cout<<""<<endl;
  
	}
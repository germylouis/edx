/* Name: Germy Felder Louis
Date: 11/9/2015
Section: 12
Assignment: Assignment #6 
Due Date: 11/10/2015
About this project:  Asks for password and keeps asking if criteria is not met
Assumptions: I assume that this project will prompt the user for a password.

All work below was performed by Germy Louis */
#include <iostream>
#include <cstring>
using namespace std;
bool pwcheck(char[]);//declaring function
int main ()
{
	const int SIZE = 80;//setting size of array
	char line[SIZE];//initial passsword
	char line2[SIZE];//password to compare to initial
	int l;
	

	cout<<"Enter a password no more than "<<(SIZE-1)<<" characters:";//prompting user
	cin.getline(line,SIZE);
	cout<<endl;
	l = strlen(line);

		while(pwcheck(line)==false){//checking if password requirement is not met
			pwcheck(line);
			cout<<endl;
			cout<<"Enter password:";
			cin.getline(line,SIZE);
			
			}
		if(pwcheck(line)==true){//checking for password requirements
			cout<<endl;
			cout<<"Now re-enter your password for verification:";
			cin.getline(line2,SIZE);
		}
			 if(strcmp(line,line2)>0 || strcmp(line,line2)<0){//comparing lines
				cout<<"Password does not match.  Start over"<<endl;
				cout<<endl;
				cout<<"Enter Password";
				cin.getline(line,SIZE);
				cout<<endl;
				pwcheck(line);
	
	}

	return 0;
	
}
bool pwcheck (char werd[])//defining function
{
	bool upcount=0, lowcount=0, intcount=0;//counters 
	for(int i=0;i<strlen(werd); i++)
	{
		if(isupper(werd[i]))
			upcount=1;
		if(isdigit(werd[i]))
			intcount=1;
		if(islower(werd[i]))
			lowcount=1;

	}
	if(lowcount<1)//if password does not have lower case
		cout<<"Password needs to contain at least one lowercase letter"<<endl;
	
	if(intcount<1)//if password does not have digit
		cout<<"Password needs to contain at least one digit"<<endl;
	
	if(upcount<1)//if password does not have uppercase
		cout<<"Password needs to contain at least one uppercase letter"<<endl;
	
	if(strlen(werd)<6)//if password is long enough
		cout<<"Password needs to have 6 or more characters"<<endl;
	
	if(intcount>=1 && lowcount>=1 && upcount>=1 && strlen(werd)>=6)//if req are met

		return true;
	
	else
		return false;
	
}
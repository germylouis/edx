/*
Germy Louis
March 25,2016
COP 3014: Box Assignment 7
*/
#include <iostream>
using std::cout;
using namespace std;
#include "Box.h"		//inlucind box.h file

 Box::Box (  int si, char bord, char fill){
	 setBox(si , bord , fill);}

void Box::setBox(int s,char b,char f){
	setlength (s);
	SetBorder(b);
	SetFill(f);
	}
void Box::Shrink()		//shrinks box 
 {
	 if(size-1<1){
		 size=size;
	 }
	 else{
		size=size-1;
	 }
 }
void Box::Grow()	//grows box
 {
	 if(size+1>39){
		 size=size;
	 }
	 else{
		size=size+1;
	 }
	 
 }
void Box::SetBorder(char bord){   //setting boarder
	if(bord<'!'||bord>'~'){
		border='#';
	}
	else 
	{
		border=bord;
	}
}
	
void Box::SetFill(char fill){   //getting the fill character
	if(fill<'!'||fill>'~'){
	filling='*';
	}
	else{ 
		filling=fill;
	}
}

void Box::setlength(int s){   //getting the size
if (s<1)
	size = 1;
if(s>39)
	size = 39;
else
size = s;
}
int Box::GetSize()   //returning size
{
	return size;
}
int Box::Area()   //returning the area
{
	return size*size;
}
int Box::Perimeter ()		//returning the perimeter
{
	int perm;
	perm= (size + size + size + size);
	return perm;
}
void Box::Draw() {   //drawing my box
	setlength(size);
	int count2=0;
	for(int i=0;i<size;i++){  //loop for box drawing
		cout<<border;
		count2++;
	}
	cout<<endl;
	for(int i=0;i<size-2;i++){
	cout<<border;
			for(int i=0;i<size-2;i++){  //nested  for loop for filling
				cout<<filling;
									}
	cout<<border<<endl;
	}
	for(int i=0;i<size;i++){
		cout<<border;   //setting the bottom border
	}
	cout<<endl;
}
void Box::Summary(){   //draw function definition
	cout<<"The box size is "<< GetSize() << endl;
	cout<<"The perimeter is "<<Perimeter()<<endl;
	cout<<"The area is "<<Area()<<endl;
	Draw();
}